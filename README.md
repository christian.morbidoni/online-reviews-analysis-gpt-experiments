# Online-reviews-analyisis-GPT-Experiments

The data in this repository consists of a selection of product customer reviews 
and was used in the upcoming Poster paper:

Christian Morbidoni, 
**"LLMs for online customer reviews analysis: oracles or tools? Experiments with GPT 3.5"**,
CHItaly 2023, September 2023, Turin, Italy

![image info](images/schema_results.jpg){width=70%}

.. more details to be shared soon.


